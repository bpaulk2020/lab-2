// Brody Paulk
//Section 04
//Lab 2


#include<iostream> 
#include <cstdlib>
#include <string>
using namespace std;  

string inputchoice;
int nthnumber;
int gennumber;
int guess;
int attempts = 1;

//formula used to calculate nth number using input
int partA1(int nthnumber) {
   if((nthnumber==1)||(nthnumber==0)) {
      return(nthnumber);
   }else {
      return(partA1(nthnumber-1)+partA1(nthnumber-2));
   }
}

//Part A: Choose nth number and prints value of nth number
void partA() {
    cout << "Activating Fibonacci" << endl;
    cout << "Enter the nth number you want to compute: ";
    cin >> nthnumber;
    cout << "The nth Fibonacci number is: " << partA1(nthnumber);
}

//Part B: Generates random number and outputs higher/lower on guess, correct on correct guess, and records tries and prints number of tries
void partB() {
    cout << "Playing guess the number game" << endl;
    srand(time(nullptr));
    gennumber= rand() % 100;
    cout << "Guess a number between 0 & 99: ";
    cin >> guess;
        do {
            if(guess < gennumber) {
                cout << "The number is higher" << endl;
                cin >> guess;
                ++attempts;
            }
            else if (guess > gennumber) {
                cout << "The number is lower" << endl;
                cin >> guess;
                ++attempts;
            }
            else {
                break;
            }
            } while (guess != gennumber);

    cout << "You guessed the correct number. " << "You tried " << attempts << " times." << endl;
}

//Part Prime: Choose Fibonacci or Guessing Game
int main() {
cout << "What would you like to do? " << endl; 
cout << "1.Enter Fibonacci to compute the nth Fibonacci number." << endl; 
cout << "2. Enter Game to play a guessing game." << endl;
cin >> inputchoice;
cout << "Your choice:" << inputchoice << endl;
    if(inputchoice=="Fibonacci") {
        partA(); 
    }
     else if(inputchoice=="Game") {
        partB();
    }
     else {
         cout << "Invalid Input";
     }
}
